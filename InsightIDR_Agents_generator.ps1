Add-Type -Path ".\WixToolset.Dtf.WindowsInstaller.dll";
$companyName = Read-Host -Prompt 'Paste the name of the company. This name will be appended to the resulting MSI file.';
Write-Host "Company name is: $companyName";
Write-Host "---------------------------";
Write-Host "Paste the full path to an unmodified MSI file.";
$MSIfilePath = Read-Host -Prompt 'Press enter to use the default - agentInstaller-x86_64.msi';
if ($MSIfilePath.Length -eq 0){$MSIfilePath = $(Get-ChildItem |Where-Object -Property Name -EQ agentInstaller-x86_64.msi)}
Write-Host "MSI path is: $MSIfilePath";
Write-Host "---------------------------";
$NewMSI = $MSIfilePath -replace ".msi","_$companyName.msi";
Copy-Item -Path $MSIfilePath -Destination $NewMSI;
$wixobj = New-Object WixToolset.Dtf.WindowsInstaller.Database("$NewMSI", [WixToolset.Dtf.WindowsInstaller.DatabaseOpenMode]::Direct);
$token = Read-Host -Prompt 'Paste the token including the geo location';
Write-Host "Token is: $token";
Write-Host "---------------------------";
[string]$wixComdToken = "INSERT INTO Property (Property,Value) VALUES ('CUSTOMTOKEN','$token')";
[string]$wixComdCnfPath = "INSERT INTO Property (Property,Value) VALUES ('CUSTOMCONFIGPATH','C:\Windows\Temp')";
$wixobj.Execute($wixComdToken);
$wixobj.Execute($wixComdCnfPath);
$wixobj.Close();
$wixobj.Dispose();
Write-Host "Modified MSI is: $NewMSI";
Write-Host "---------------------------";