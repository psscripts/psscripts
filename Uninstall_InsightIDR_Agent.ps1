#This section will gather a list of all installed applications and look for "Rapid7 Insight Agent" GUID.
#The GUID is then used as an argument with MSIEXE
$Installer = New-Object -ComObject WindowsInstaller.Installer;
$InstallerProducts = $Installer.ProductsEx("", "", 7);
$InstalledProducts = ForEach($Product in $InstallerProducts){[PSCustomObject]@{ProductCode = $Product.ProductCode();
LocalPackage = $Product.InstallProperty("LocalPackage");
VersionString = $Product.InstallProperty("VersionString");
ProductPath = $Product.InstallProperty("ProductName")}};
$R7GUID = ($InstalledProducts|Where-Object -Property ProductPath -Match "Rapid7 Insight Agent").ProductCode;
#
#Argument list for MSIEXEC is being constructed below:
$ArgList = '/x ',$R7GUID,' /qn' -join ''
#
#Executing MSIEXEC in order to uninstall the Agent
Start-Process msiexec.exe -Wait -ArgumentList $ArgList
