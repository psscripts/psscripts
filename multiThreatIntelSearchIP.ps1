$varip=$args[0]
#$varip = Read-Host -Prompt 'Please enter the IP address'

start https://check.spamhaus.org/not_listed/?searchterm=$varip
start https://ipinfo.io/$varip
start https://isc.sans.edu/ipinfo.html?ip=$varip
start https://otx.alienvault.com/indicator/ip/$varip
start https://scamalytics.com/ip/$varip
start https://talosintelligence.com/reputation_center/lookup?search=$varip
start https://www.abuseipdb.com/check/$varip
start https://www.cyber45.com/ip-blacklist?queryParam=$varip
start https://www.ipqualityscore.com/ip-reputation-check/lookup/$varip
start https://www.joesandbox.com/search?q=$varip
start https://www.malwares.com/report/ip?ip=$varip
start https://www.shodan.io/host/$varip
start https://www.virustotal.com/gui/ip-address/$varip
