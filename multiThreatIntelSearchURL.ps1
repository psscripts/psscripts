$varurl=$args[0]
#$varurl = Read-Host -Prompt 'Please enter the domain name / URL'

start https://check.spamhaus.org/not_listed/?searchterm=$varurl
start https://isc.sans.edu/ipinfo.html?ip=$varurl
start https://otx.alienvault.com/indicator/domain/$varurl
start https://sitecheck.sucuri.net/results/$varurl
start https://talosintelligence.com/reputation_center/lookup?search=$varurl
start https://urlhaus.abuse.ch/browse.php?search=$varurl
start https://www.joesandbox.com/search?q=$varurl
start https://www.malwares.com/report/host?host=$varurl
start https://www.urlvoid.com/scan/$varurl
start https://www.virustotal.com/gui/domain/$varurl
