$in=$args[0]
$table=
@{
"00000001-0000-0000-c000-000000000000" = "Azure ESTS Service"
"00000002-0000-0000-c000-000000000000" = "Microsoft Azure ActiveDirectory"
"00000002-0000-0ff1-ce00-000000000000" = "Office 365 Exchange Online"
"00000003-0000-0000-c000-000000000000" = "Microsoft Graph"
"00000003-0000-0ff1-ce00-000000000000" = "Office 365 SharePoint Online"
"00000004-0000-0ff1-ce00-000000000000" = "Skype for Business Online"
"00000005-0000-0000-c000-000000000000" = "Microsoft Azure Workflow"
"00000005-0000-0ff1-ce00-000000000000" = "Microsoft YammerEnterprise"
"00000006-0000-0ff1-ce00-000000000000" = "Microsoft Office 365 Portal"
"00000007-0000-0000-c000-000000000000" = "Dynamics CRM Online"
"00000007-0000-0ff1-ce00-000000000000" = "Microsoft Exchange Online Protection"
"00000008-0000-0000-c000-000000000000" = "Microsoft Azure DataMarket"
"00000009-0000-0000-c000-000000000000" = "Microsoft Azure AnalysisServices"
"0000000a-0000-0000-c000-000000000000" = "Microsoft Intune"
"0000000b-0000-0000-c000-000000000000" = "Microsoft Seller Dashboard"
"0000000c-0000-0000-c000-000000000000" = "Microsoft App Access Panel"
"0000000f-0000-0000-c000-000000000000" = "Microsoft Azure GraphExplorer"
"00000010-0000-0000-c000-000000000000" = "Microsoft Azure GraphStore"
"00000012-0000-0000-c000-000000000000" = "Microsoft Rights Management Services"
"00000013-0000-0000-c000-000000000000" = "Microsoft Azure Portal"
"00000014-0000-0000-c000-000000000000" = "Microsoft Azure SyncFabric"
"0000001a-0000-0000-c000-000000000000" = "MicrosoftAzureActiveAuthn"
"0000001b-0000-0000-c000-000000000000" = "Microsoft Power BI Information Service"
"0004c632-673b-4105-9bb6-f3bbd2a927fe" = "PowerApps and Flow"
"00695ed2-3202-4156-8da1-69f60065e255" = "Microsoft Visio Data Visualizer"
"00b41c95-dab0-4487-9791-b9d2c32c80f2" = "Office 365 Management"
"00edd498-7c0c-4e68-859c-5a55d518c9c0" = "Teams Calling Meeting Devices Services"
"00f82732-f451-4a01-918c-0e9896e784f9" = "Skype for Business Application Configuration Service"
"0130cc9f-7ac5-4026-bd5f-80a08a54e6d9" = "Azure Data Warehouse Polybase"
"01cb2876-7ebd-4aa4-9cc9-d28bd4d359a9" = "Device Registration Service"
"01fc33a7-78ba-4d2f-a4b7-768e336e890e" = "MS-PIM"
"022907d3-0f1b-48f7-badc-1ba6abab6d66" = "Azure SQL Database"
"02e3ae74-c151-4bda-b8f0-55fbf341de08" = "Application Registration Portal"
"035f9e1d-4f00-4419-bf50-bf2d87eb4878" = "Azure Monitor Restricted"
"04436913-cf0d-4d2a-9cc6-2ffe7f1d3d1c" = "Windows Notification Service"
"04687a56-4fc2-4e36-b274-b862fb649733" = "MDATPNetworkScanAgent"
"0469d4cd-df37-4d93-8a61-f8c75b809164" = "Microsoft Policy Administration Service"
"05a65629-4c1b-48c1-a78b-804c4abdd4af" = "Microsoft Cloud App Security"
"06dd8193-75af-46d0-84bb-9b9bcaa89e8b" = "SubstrateActionsService"
"07978fee-621a-42df-82bb-3eabc6511c26" = "SurveyMonkey"
"087a2c70-c89e-463f-8dd3-e3959eabb1a9" = "Microsoft Profile Service Platform Service"
"08e18876-6177-487e-b8b5-cf950c1e598c" = "SharePoint Online Web Client Extensibility"
"09a984f4-014c-43de-ae0c-7ec73dc053d3" = "BenefitsFD"
"09abbdfd-ed23-44ee-a2d9-a627aa1c90f3" = "ProjectWorkManagement"
"0a0a29f9-0a25-49c7-94bf-c53c3f8fa69d" = "Cortana Experience with O365"
"0a0e9e37-25e3-47d4-964c-5b8237cad19a" = "CloudSponge"
"0a5f63c0-b750-4f38-a71c-4fc0d58b89e2" = "Microsoft Mobile Application Management"
"0bf30f3b-4a52-48df-9a82-234910c4a086" = "Microsoft Graph Change Tracking"
"0c708d37-30b2-4f22-8168-5d0cba6f37be" = "Microsoft Teams Partner Tenant Administration"
"0c8139b5-d545-4448-8d2b-2121bb242680" = "BillingExtension"
"0cd196ee-71bf-4fd6-a57c-b491ffd4fb1e" = "Media Analysis and Transformation Service"
"0cd79364-7a90-4354-9984-6e36c841418d" = "Access IoT Hub Device Provisioning Service"
"0d38933a-0bbd-41ca-9ebd-28c4b5ba7cb7" = "Office365 Zoom"
"0eb4bf93-cb63-4fe1-9d7d-70632ccf3082" = "Microsoft Teams Intelligent Workspaces Interactions Service"
"0eda3b13-ddc9-4c25-b7dd-2f6ea073d6b7" = "Microsoft Flow CDS Integration Service"
"0f54b75d-4d29-4a92-80ae-106a60cd8f5d" = "Teams User Engagement Profile Service"
"0f698dd4-f011-4d23-a33e-b36416dcb1e6" = "Microsoft OfficeClientService"
"0f6e3eff-886c-4f7a-a1d7-6f1f0177273b" = "Intune oAuth Graph"
"0f6edad5-48f2-4585-a609-d252b1c52770" = "AIGraphClient"
"0fa37baf-7afc-4baf-ab2d-d5bb891d53ef" = "Microsoft Teams ATP Service"
"11c174dc-1945-4a9a-a36b-c79a0f246b9b" = "Windows Azure Application Insights"
"11cd3e2e-fccb-42ad-ad00-878b93575e07" = "Automated Call Distribution"
"123cd850-d9df-40bd-94d5-c9f07b7fa203" = "Azure OSSRDBMS Database"
"12743ff8-d3de-49d0-a4ce-6c91a4245ea0" = "Metrics Monitor API"
"13d54852-ae25-4f0b-823a-b09eea89f431" = "Outlook Service for Exchange"
"14452459-6fa6-4ec0-bc50-1528a1a06bf0" = "Intune CMDeviceService"
"15689b28-1333-4213-bb64-38407dde8a5e" = "Microsoft Azure"
"159b90bb-bb28-4568-ad7c-adad6b814a2f" = "LastPass"
"166f1b03-5b19-416f-a94b-1d7aa2d247dc" = "Office Hive"
"167e2ded-f32d-49f5-8a10-308b921bc7ee" = "OCaaS Worker Services"
"17ef6d31-381f-4783-b186-7b440a3c85c1" = "Workflow"
"184909ca-69f1-4368-a6a7-c558ee6eb0bd" = "Marketplace Caps API"
"18a4ad1e-427c-4cad-8416-ef674e801d32" = "Intune DeviceActionService"
"18f36947-75b0-49fb-8d1c-29584a55cac5" = "Reply-At-Mention"
"19686ca6-5324-4571-a231-77e026b0e06f" = "Microsoft Command Service"
"19947cfd-0303-466c-ac3c-fcc19a7a1570" = "Azure DNS"
"1996141e-2b07-4491-927a-5a024b335c78" = "Microsoft Teams UIS"
"1a14be2a-e903-4cec-99cf-b2e209259a0f" = "Azure Lab Services"
"1b730954-1685-4b74-9bfd-dac224a7b894" = "Global Powershell Multitenant"
"1c0ae35a-e2ec-4592-8e08-c40884656fa5" = "Skype Team Substrate connector"
"1c2909a7-6432-4263-a70d-929a3c1f9ee5" = "Common Data Service License Management"
"1c827867-9069-4bde-8155-1a0267a3dea5" = "Microsoft DataMovement Metadata Service"
"1cda9b54-9852-4a5a-96d4-c2ab174f9edf" = "O365Account"
"1d78a85d-813d-46f0-b496-dd72f50a3ec0" = "Microsoft Azure Policy Insights"
"1e2ca66a-c176-45ea-a877-e87f7231e0ee" = "Microsoft B2B Admin Worker"
"1e3e4475-288f-4018-a376-df66fd7fac5f" = "NetworkTrafficAnalyticsService"
"1e70cd27-4707-4589-8ec5-9bd20c472a46" = "Skype Presence Service"
"1f5530b3-261a-47a9-b357-ded261e17918" = "Azure Multi-Factor Auth Connector"
"1fe0d6b3-81f0-4cf5-9dfd-fbb297d7848c" = "Microsoft Insider Risk Management"
"1fec8e78-bce4-4aaf-ab1b-5451cc387264" = "Microsoft Teams"
"207a6836-d031-4764-a9d8-c1193f455f21" = "Conference Auto Attendant"
"2087bd82-7206-4c0a-b305-1321a39e5926" = "Microsoft To-Do"
"20a23a2f-8c32-4de7-8063-8c8f909602c0" = "Workflow"
"21a8a852-89f4-4947-a374-b26b2db3d365" = "IC3 Long Running Operations Service"
"224a7b82-46c9-4d6b-8db0-7360fb444681" = "Microsoft Modern Contact Master"
"22d7579f-06c2-4baa-89d2-e844486adb9d" = "Cortana at Work Bing Services"
"23c898c1-f7e8-41da-9501-f16571f8d097" = "OCPS Checkin Service"
"257601fd-462f-4a21-b623-7f719f0f90f4" = "Centralized Deployment"
"262044b1-e2ce-469f-a196-69ab7ada62d3" = "Backup Management Service"
"2634dd23-5e5a-431c-81ca-11710d9079f4" = "Microsoft Stream Service"
"26a18ebc-cdf7-4a6a-91cb-beb352805e81" = "Skype Teams Calling API Service"
"26a4ae64-5862-427f-a9b0-044e62572a4f" = "Microsoft Intune Checkin"
"2728b157-fe96-4203-a49f-cc31c93a2ba3" = "Outlook Service for OneDrive"
"273404b8-7ebc-4360-9f90-b40417f77b53" = "Microsoft Exact Data Match Service"
"2746ea77-4702-4b45-80ca-3c97e680e8b7" = "KustoService"
"27922004-5251-4030-b22d-91ecd9a37ea4" = "Microsoft Outlook for Android APK"
"2793995e-0a7d-40d7-bd35-6968ba142197" = "My Apps"
"27b24f1f-688b-4661-9594-0fdfde972edc" = "Skype Business Voice Directory"
"28ec9756-deaf-48b2-84d5-a623b99af263" = "Office Personal Assistant at Work Service"
"29d9ed98-a469-4536-ade2-f981bc1d605e" = "Airwatch MDM app"
"29f411f1-b2cf-4043-8ac8-2185d7316811" = "Azure Iot Hub Publisher App"
"2a486b53-dbd2-49c0-a2bc-278bdfc30833" = "Cortana at Work Service"
"2bb78a2a-f8f1-4bc3-8ecf-c1e15a0726e6" = "MicrosoftTeamsCortanaSkills"
"2cf9eb86-36b5-49dc-86ae-9a63135dfa8c" = "Azure Traffic Manager and DNS"
"2d4d3d8e-2be3-4bef-9f87-7875a61c29de" = "OneNote"
"2db8cb1d-fb6c-450b-ab09-49b6ae35186b" = "Microsoft Dynamics CRM Learning Path"
"2e49aa60-1bd3-43b6-8ab6-03ada3d9f08b" = "Dynamics Data Integration"
"2f3f02c9-5679-4a5c-a605-0de55b07d135" = "Office 365 Information Protection"
"2f5afa01-cdcb-4707-a62a-0803cc994c60" = "MS-CE-CXG-MAC-AadShadowRoleWriter"
"30e31aeb-977f-4f4f-a483-b61e8377b302" = "Microsoft Teams ADL"
"3138fe80-4087-4b04-80a6-8866c738028a" = "SharePoint Notification Service"
"3184af01-7a88-49e0-8b55-8ecdce0aa950" = "Azure Cost Management XCloud"
"31d3f3f5-7267-45a8-9549-affb00110054" = "Windows Azure RemoteApp Service"
"32613fc5-e7ac-4894-ac94-fbc39c9f3e4a" = "OAuth Sandbox"
"328fd23b-de6e-462c-9433-e207470a5727" = "NFV Resource Provider"
"3290e3f7-d3ac-4165-bcef-cf4874fc4270" = "Smartsheet"
"331cc017-5973-4173-b270-f0042fddfd75" = "PowerAppsService"
"3340b944-b12e-47d0-b46b-35f08ec1d8ee" = "SPAuthEvent"
"33be1cef-03fb-444b-8fd3-08ca1b4d803f" = "OneDrive Web"
"341b7f3d-69b3-47f9-9ce7-5b7f4945fdbd" = "Azure Support - Network Watcher"
"342f61e2-a864-4c50-87de-86abc6790d49" = "Power Platform Governance Services"
"354b5b6d-abd6-4736-9f51-1be80049b91f" = "Microsoft Mobile Application Management Backend"
"36e2398c-9dd3-4f29-9a72-d9f2cfc47ad9" = "CosmosDB Dedicated Instance"
"37182072-3c9c-4f6a-a4b3-b3f91cacffce" = "AzureSupportCenter"
"372140e0-b3b7-4226-8ef9-d57986796201" = "Azure Windows VM Sign-In"
"38285dce-a13d-4107-9b04-3016b941bb3a" = "BasicDataOperationsREST"
"38aa3b87-a06d-4817-b275-7a316988d93b" = "Dynamics 365"
"394866fc-eedb-4f01-8536-3ff84b16be2a" = "Microsoft People Cards Service"
"39624784-6cbe-4a60-afbe-9f46d10fdb27" = "SkypeForBusinessRemotePowershell"
"396e7f4b-41ea-4851-b04d-65de6cf1b4a3" = "Azure AD Identity Governance - SPO Management"
"39aaf054-81a5-48c7-a4f8-0293012095b9" = "IC3 Gateway"
"39e6ea5b-4aa4-4df2-808b-b6b5fb8ada6f" = "Dynamics Provision"
"3a9ddf38-83f3-4ea1-a33a-ecf934644e2d" = "Protected Message Viewer"
"3aa5c166-136f-40eb-9066-33ac63099211" = "O365 Customer Monitoring"
"3ab9b3bc-762f-4d62-82f7-7e1d653ce29f" = "Microsoft Volume Licensing"
"3af5a1e8-2459-45cb-8683-bcd6cccbcc13" = "Azure Smart Alerts"
"3b2fa68d-a091-48c9-95be-88d572e08fb7" = "AzureBackupReporting"
"3c31d730-a768-4286-a972-43e9b83601cd" = "Demeter WorkerRole"
"3cf6df92-2745-4f6f-bbcf-19b59bcdb62a" = "Office 365 Client Admin"
"3e050dd7-7815-46a0-8263-b73168a42c10" = "Teams Approvals"
"3eb95cef-b10f-46fe-94e0-969a3d4c9292" = "Office 365 Import Service"
"40775b29-2688-46b6-a3b5-b256bd04df9f" = "Microsoft Information Protection API"
"433895fb-4ec7-45c3-a53c-c44d10f80d5b" = "Compromised Account Service"
"4345a7b9-9a63-4910-a426-35363201d503" = "O365 Suite UX"
"4353526e-1c33-4fcf-9e82-9683edf52848" = "ConfidentialLedger"
"441509e5-a165-4363-8ee7-bcf0b7d26739" = "EnterpriseAgentPlatform"
"443155a6-77f3-45e3-882b-22b3a8d431fb" = "Domain Controller Services"
"44a02aaa-7145-4925-9dcd-79e6e1b94eff" = "Microsoft Dynamics 365 Apps Integration"
"44eb7794-0e11-42b6-800b-dc31874f9f60" = "Alignable"
"4580fd1d-e5a3-4f56-9ad1-aab0e3bf8f76" = "Call Recorder"
"45a330b1-b1ec-4cc1-9161-9f03992aa49f" = "Windows Store for Business"
"45c10911-200f-4e27-a666-9e9fca147395" = "drawio"
"461e8683-5575-4561-ac7f-899cc907d62a" = "Azns AAD Webhook"
"4660504c-45b3-4674-a709-71951a6b0763" = "Microsoft Invitation Acceptance Portal"
"4747d38e-36c5-4bc3-979b-b0ef74df54d1" = "PushChannel"
"475226c6-020e-4fb2-8a90-7a972cbfc1d4" = "PowerApps Service"
"47ee738b-3f1a-4fc7-ab11-37e4822b007e" = "Azure AD Application Proxy"
"48229a4a-9f1d-413a-8b96-4c02462c0360" = "OpsDashSharePointApp"
"486c78bf-a0f7-45f1-92fd-37215929e116" = "GatewayRP"
"48717084-a59c-4306-9dc4-3f618dbecdf9" = "Napa Office 365 Development Tools"
"488a57a0-00e2-4817-8c8d-cf8a15a994d2" = "WindowsFormsApplication2 Office365App"
"48ac35b8-9aa8-4d74-927d-1f4a14a0b239" = "Skype and Teams Tenant Admin API"
"48af08dc-f6d2-435f-b2a7-069abd99c086" = "Connectors"
"4990cffe-04e8-4e8b-808a-1175604b879f" = "Microsoft Dev Center"
"499b84ac-1321-427f-aa17-267ca6975798" = "Microsoft VisualStudio Online"
"4ac7d521-0382-477b-b0f8-7e1d95f85ca2" = "SQL Server Analysis Services Azure"
"4b4b1d56-1f03-47d9-a0a3-87d4afc913c9" = "Wunderlist"
"4bfd5d66-9285-44a1-bb14-14953e8cdf5e" = "Audit GraphAPI Application"
"4c4f550b-42b2-4a16-93f9-fdb9e01bb6ed" = "Targeted Messaging Service"
"4c8f074c-e32b-4ba7-b072-0f39d71daf51" = "IPSubstrate"
"4d0ad6c7-f6c3-46d8-ab0d-1406d5e6c86b" = "Azure Reserved Instance Application"
"4e004241-32db-46c2-a86f-aaaba29bea9c" = "Workflow"
"4e445925-163e-42ca-b801-9073bfa46d17" = "SharePoint Home Notifier"
"4e9b8b9a-1001-4017-8dd1-6e8f25e19d13" = "Adobe Acrobat"
"507bc9da-c4e2-40cb-96a7-ac90df92685c" = "Office 365 Reports"
"509e4652-da8d-478d-a730-e9d4a1996ca4" = "Azure Resources Topology"
"510a5356-1745-4855-93a5-113ea589fb26" = "Microsoft 365 Ticketing"
"51133ff5-8e0d-4078-bcca-84fb7f905b64" = "Microsoft Teams Mailhook"
"51df634f-ddb4-4901-8a2d-52f6393a796b" = "Azure Region Move Orchestrator Application"
"5225545c-3ebd-400f-b668-c8d78550d776" = "Office Agent Service"
"546068c3-99b1-4890-8e93-c8aeadcfe56a" = "Common Data Service - Azure Data Lake Storage"
"55441455-2f54-42b5-bc99-93e21cd4ae28" = "Office Enterprise Protection Service"
"557c67cf-c916-4293-8373-d584996f60ae" = "Configuration Manager Microservice"
"562db366-1b96-45d2-aa4a-f2148cef2240" = "Diagnostic Services Trusted Storage Access"
"5635d99c-c364-4411-90eb-764a511b5fdf" = "Responsive Banner Slider"
"569e8598-685b-4ba2-8bff-5bced483ac46" = "Evercontact"
"56c1da01-2129-48f7-9355-af6d59d42766" = "Graph Connector Service"
"57084ef3-d413-4087-a28f-f6f3b1ad7786" = "Branch Connect Web Service"
"57336123-6e14-4acc-8dcf-287b6088aa28" = "Microsoft Whiteboard application"
"579d9c9d-4c83-4efc-8124-7eba65ed3356" = "Azure Compute"
"57c0fc58-a83a-41d0-8ae9-08952659bdfd" = "Azure Cosmos DB Virtual Network To Network Resource Provider"
"57fb890c-0dab-4253-a5e0-7188c88b2bb4" = "SharePoint Online Client"
"589d5083-6f11-4d30-a62a-a4b316a14abf" = "Azure Key Vault Managed HSM"
"58c746b0-a0b0-4647-a8f6-12dde5981638" = "Azure AD Identity Governance Insights"
"58ea322b-940c-4d98-affb-345ec4cccb92" = "M365 Pillar Diagnostics Service"
"595d87a1-277b-4c0a-aa7f-44f8a068eafc" = "Microsoft SupportTicketSubmission"
"5b20c633-9a48-4a5f-95f6-dae91879051f" = "Azure Information Protection"
"5bfe8a29-054e-4348-9e7a-3981b26b125f" = "Bing Places for Business"
"5c2ffddc-f1d7-4dc3-926e-3c1bd98e32bd" = "RITS Dev"
"5da7367f-09c8-493e-8fd4-638089cddec3" = "CABProvisioning"
"5e3ce6c0-2b1f-4285-8d4b-75ee78787346" = "Microsoft Teams Web Client"
"5e5e43d4-54da-4211-86a4-c6e7f3715801" = "Azure Regional Service Manager"
"601d4e27-7bb3-4dee-8199-90d47d527e1c" = "Microsoft Office365 ChangeManagement"
"603b8c59-ba28-40ff-83d1-408eee9a93e5" = "Portable Identity Card Service"
"607e1f95-b519-4bac-8a15-6196f40e8977" = "StreamToSubstrateRepl"
"60e6cd67-9c8c-4951-9b3c-23c25a2169af" = "Compute Resource Provider"
"6201d19e-14fb-4472-a2d6-5634a5c97568" = "Event Hub MSI App"
"6204c1d1-4712-4c46-a7d9-3ed63d992682" = "Microsoft Flow Portal"
"6208afad-753e-4995-bbe1-1dfd204b3030" = "Teams ACL management service"
"62b732f7-fc71-40bc-b27d-35efcb0509de" = "Microsoft Teams AadSync"
"62c559cd-db0c-4da0-bab2-972528c65d42" = "ACR-Tasks-Network"
"62fd1447-0ef3-4ab7-a956-7dd05232ecc1" = "Office Scripts Service"
"63e61dc2-f593-4a6f-92b9-92e4d2c03d4f" = "Microsoft Intune SCCM Connector"
"64a7b174-5779-4506-b54c-fbb0d80f1c9b" = "CMAT"
"64f79cb9-9c82-4199-b85b-77e35b7dcbcb" = "Microsoft Teams Bots"
"65d91a3d-ab74-42e6-8a2f-0add61688c74" = "Microsoft Approval Management"
"660d4be7-2665-497f-9611-a42c2668dbce" = "Microsoft Fluid Framework Preview"
"66244124-575c-4284-92bc-fdd00e669cea" = "IAMTenantCrawler"
"6682cfa5-2710-44c9-adb8-5ac9d76e394a" = "MTS"
"66a88757-258c-4c72-893c-3e8bed4d6899" = "Office 365 Search Service"
"66c6d0d1-f2e7-4a18-97a9-ed10f3347016" = "Managed Service"
"67cad61c-3411-48d7-ab73-561c64f11ed6" = "Workflow"
"67e3df25-268a-4324-a550-0de1c7f97287" = "Microsoft Office Web Apps Service"
"6a0a243c-0886-468a-a4c2-eff52c7445da" = "Application Insights Configuration Service"
"6a0ec4d3-30cb-4a83-91c0-ae56bc0e3d26" = "Azure Container Registry"
"6b91db1b-f05b-405a-a0b2-e3f60b28d645" = "M365 Admin Services"
"6bccf540-eb86-4037-af03-7fa058c2db75" = "Geneva Alert RP"
"6d057c82-a784-47ae-8d12-ca7b38cf06b4" = "Networking-MNC"
"6d32b7f8-782e-43e0-ac47-aaad9f4eb839" = "Permission Service O365"
"6da466b6-1d13-4a2c-97bd-51a99e8d4d74" = "Exchange Office Graph Client for AAD - Interactive"
"6dae42f8-4368-4678-94ff-3960e28e3630" = "Azure Kubernetes Service AAD Server"
"6e99704e-62d5-40f6-b2fe-90aafbe3a710" = "OCaaS Experience Management Service"
"6ea8091b-151d-447a-9013-6845b83ba57b" = "AD Hybrid Health"
"6f0478d5-61a3-4897-a2f2-de09a5a90c7f" = "WindowsUpdate-Service"
"6f7d0213-62b1-43a8-b7f4-ff2bb8b7b452" = "Dual-write"
"6f82282e-0070-4e78-bc23-e6320c5fa7de" = "Microsoft Discovery Service"
"707be275-6b9d-4ee7-88f9-c0c2bd646e0f" = "Dynamic Alerts"
"709110f7-976e-4284-8851-b537e9bcb187" = "Microsoft Device Management Enrollment"
"71234da4-b92f-429d-b8ec-6e62652e50d7" = "Microsoft Customer Engagement Portal"
"71a7c376-13e6-4100-968e-92ce98c5d3d2" = "Weve"
"728a93e3-065d-4678-93b1-3cc281223341" = "MicrosoftOffAzureApp"
"7319c514-987d-4e9b-ac3d-d38c4f427f4c" = "AzureContainerService"
"737d58c1-397a-46e7-9d12-7d8c830883c2" = "Azure Marketplace Container Management API"
"73c2949e-da2d-457a-9607-fcc665198967" = "Microsoft Project Babylon"
"744e50be-c4ff-4e90-8061-cd7f1fabac0b" = "LinkedIn Microsoft Graph Connector"
"748d098e-7a3b-436d-8b0a-006a58b29647" = "Workflow"
"74bcdadc-2fdc-4bb3-8459-76d06952a0e9" = "Microsoft Intune Web Company Portal"
"75018fbe-21fe-4a57-b63c-83252b5eaf16" = "TeamImprover - Team Organization Chart"
"75513c96-801d-4559-830a-6754de13dd19" = "M365 Label Analytics"
"7557eb47-c689-4224-abcf-aef9bd7573df" = "Skype for Business"
"75efb5bc-18a1-4e7b-8a66-2ad2503d79c6" = "Microsoft Teams Retail Service"
"765fe668-04e7-42ba-aec0-2c96f1d8b652" = "Exchange Office Graph Client for AAD - Noninteractive"
"766d89a4-d6a6-444d-8a5e-e1a18622288a" = "OneDrive"
"76c7f279-7959-468f-8943-3954880e0d8c" = "Azure SQL Managed Instance to Microsoft Network"
"76cd24bf-a9fc-4344-b1dc-908275de6d6d" = "Azure SQL Virtual Network to Network Resource Provider"
"78e7bc61-0fab-4d35-8387-09a8d2f5a59d" = "Yggdrasil"
"794ded15-70c6-4bcd-a0bb-9b7ad530a01a" = "Microsoft Intune Advanced Threat Protection Integration"
"797f4846-ba00-4fd7-ba43-dac1f8f63013" = "Windows Azure Service Management API"
"7ae5462d-d9d1-42f6-93ca-198d7b0ca997" = "Microsoft O365 Scuba"
"7b58f833-4438-494c-a724-234928795a67" = "Directory and Policy Cache"
"7b7531ad-5926-4f2d-8a1d-38495ad33e17" = "Azure Advanced Threat Protection"
"7b77b3a2-8490-49e1-8842-207cd0899af9" = "Nearpod"
"7c33bfcb-8d33-48d6-8e60-dc6404003489" = "Network Watcher"
"7c99d979-3b9c-4342-97dd-3239678fb300" = "Data Classification Service"
"7cd684f4-8a78-49b0-91ec-6a35d38739ba" = "Azure Logic Apps"
"7dcff627-a295-4553-9229-b1f3513f82a8" = "Power Platform Data Analytics"
"7df0a125-d3be-4c96-aa54-591f83ff541c" = "Microsoft Flow Service"
"7e468355-e4db-46a9-8289-8d414c89c43c" = "Federated Profile Service"
"7f0d9978-eb2a-4974-88bd-f22a3006fe17" = "Intune DiagnosticService"
"7f15f9d9-cad0-44f1-bbba-d36650e07765" = "Export to data lake"
"80369ed6-5f11-4dd9-bef3-692475845e77" = "Microsoft EventHubs"
"803ee9ca-3f7f-4824-bd6e-0b99d720c35c" = "Azure Media Service"
"80ccca67-54bd-44ab-8625-4b79c4dc7775" = "Protection Center"
"80dbdb39-4f33-4799-8b6f-711b5e3e61b6" = "Billing"
"810dcf14-1858-4bf2-8134-4c369fa3235b" = "Azure AD Identity Governance - Entitlement Management"
"81473081-50b9-469a-b9d8-303109583ecb" = "Cortana Runtime Service"
"81ce94d4-9422-4c0d-a4b9-3250659366ce" = "Workflow"
"823dfde0-1b9a-415a-a35a-1ad34e16dd44" = "Microsoft Teams Wiki Images Migration"
"82b293b2-d54d-4d59-9a95-39c1c97954a7" = "Tasks in a Box"
"82f77645-8a66-4745-bcdf-9706824f9ad0" = "PowerApps Runtime Service"
"8338dec2-e1b3-48f7-8438-20c30a534458" = "ViewPoint"
"844cca35-0656-46ce-b636-13f48b0eecbd" = "Microsoft Stream Mobile Native"
"8602e328-9b72-4f2d-a4ae-1387d013a2b3" = "Azure API Management"
"870c4f2e-85b6-4d43-bdda-6ed9a579b725" = "Microsoft Information Protection Sync Service"
"87749df4-7ccf-48f8-aa87-704bad0e0e16" = "Microsoft Teams - Device Admin Agent"
"88884730-8181-4d82-9ce2-7d5a7cc7b81e" = "SharePoint Notification Service"
"8909aac3-be91-470c-8a0b-ff09d669af91" = "Microsoft Parature Dynamics CRM"
"89bee1f7-5e6e-4d8a-9f3d-ecd601259da7" = "webshellsuite"
"89d10474-74af-4874-99a7-c23c2f643083" = "Azure Iot Hub"
"89f80565-bfac-4c01-9535-9f0eba332ffe" = "Power BI"
"8a753eec-59bc-4c6a-be91-6bf7bfe0bcdf" = "Teams Application Gateway"
"8ad28d50-ee26-42fc-8a29-e41ea38461f2" = "Office365RESTAPIExplorer Office365App"
"8ae6a0b1-a07f-4ec9-927a-afb8d39da81c" = "Microsoft Device Management EMM API"
"8b3391f4-af01-4ee8-b4ea-9871b2499735" = "O365 Secure Score"
"8b62382d-110e-4db8-83a6-c7e8ee84296a" = "PowerAI"
"8bbf8725-b3ca-4468-a217-7c8da873186e" = "DeploymentScheduler"
"8bdebf23-c0fe-4187-a378-717ad86f6a53" = "ResourceHealthRP"
"8bea2130-23a1-4c09-acfb-637a9fb7c157" = "M365 Pillar Diagnostics Service API"
"8c420feb-03df-47cc-8a05-55df0cf3064b" = "AzureUpdateCenter"
"8c8fbf21-0ef3-4f60-81cf-0df811ff5d16" = "Power Query Online GCC-L5"
"8cae6e77-e04e-42ce-b5cb-50d82bce26b1" = "Microsoft Policy Insights Provider Data Plane"
"8d3a7d3c-c034-4f19-a2ef-8412952a9671" = "Microsoft Office Licensing Service"
"8d40666e-5abf-45f6-a5e7-b7192d6d56ed" = "Workflow"
"8e0e8db5-b713-4e91-98e6-470fed0aa4c2" = "Microsoft Azure Signup Portal"
"8e14e873-35ba-4720-b787-0bed94370b17" = "Microsoft Teams Targeting Application"
"8edd93e1-2103-40b4-bd70-6e34e586362d" = "Windows Azure Security Resource Provider"
"8ee8fdad-f234-4243-8f3b-15c294843740" = "Microsoft Threat Protection"
"8f348934-64be-4bb2-bc16-c54c96789f43" = "EDU Assignments"
"8f41dc7c-542c-4bdd-8eb3-e60543f607ca" = "Microsoft Device Directory Service"
"8fca0a66-c008-4564-a876-ab3ae0fd5cff" = "Microsoft SMIT"
"905fcf26-4eb7-48a0-9ff0-8dcc7194b5ba" = "Sway"
"913c6de4-2a4a-4a61-a9ce-945d2b2ce2e0" = "Dynamics Lifecycle services"
"914ed757-9257-4200-b68e-a2bed2f12c5a" = "RbacBackfill"
"918d0db8-4a38-4938-93c1-9313bdfe0272" = "asmcontainerimagescanner"
"925eb0d0-da50-4604-a19f-bd8de9147958" = "Groupies Web Service"
"92876b03-76a3-4da8-ad6a-0511ffdf8647" = "ComplianceWorkbenchApp"
"92bb96c8-321c-47f9-bcc5-8849490c2b07" = "BasicSelfHostedAppREST"
"93625bc8-bfe2-437a-97e0-3d0060024faa" = "Microsoft password reset service"
"939fe80f-2eef-464f-b0cf-705d254a2cf2" = "Power Query Online GCC-L2"
"93bd1aa4-c66b-4587-838c-ffc3174b5f13" = "Power Platform Global Discovery Service"
"93ee9413-cf4c-4d4e-814b-a91ff20a01bd" = "Workflow"
"94c63fef-13a3-47bc-8074-75af8c65887a" = "Office Delve"
"959678cf-d004-4c22-82a6-d2ce549a58b8" = "Microsoft_Azure_Support"
"95de633a-083e-42f5-b444-a4295d8e9314" = "Microsoft Whiteboard Services"
"96231a05-34ce-4eb4-aa6a-70759cbb5e83" = "MicrosoftAzureRedisCache"
"96ff4394-9197-43aa-b393-6a41652e21f8" = "Dynamics 365 AI for Customer Service Bot"
"978877ea-b2d6-458b-80c7-05df932f3723" = "Microsoft Teams AuditService"
"981f26a1-7f43-403b-a875-f8b09b8cd720" = "Azure Multi-Factor Auth Client"
"982bda36-4632-4165-a46a-9863b1bbcf7d" = "O365 Demeter"
"98785600-1bb7-4fb9-b9fa-19afe2c8a360" = "Azure Security Insights"
"98c8388a-4e86-424f-a176-d1288462816f" = "OfficeFeedProcessors"
"98db8bd6-0cc0-4e67-9de5-f187f1cd1b41" = "Microsoft Substrate Management"
"99335b6b-7d9d-4216-8dee-883b26e0ccf7" = "Power Platform Dataflows Common Data Service Client"
"996def3d-b36c-4153-8607-a6fd3c01b89f" = "Dynamics 365 Business Central"
"9b06ebd4-9068-486b-bdd2-dac26b8a5a7a" = "Microsoft DynamicsMarketing"
"9ba1a5c7-f17a-4de9-a1f1-6178c8d51223" = "Intune"
"9bb724a5-4639-438c-969b-e184b2b1e264" = "KaizalaActionsPlatform"
"9bdab391-7bbe-42e8-8132-e4491dc29cc0" = "Azure Backup NRP Application"
"9cb77803-d937-493e-9a3b-4b49de3f5a74" = "Microsoft Intune Service Discovery"
"9cd0f7df-8b1a-4e54-8c0c-0ef3a51116f6" = "DirectoryLookupService"
"9d06afd9-66c9-49a6-b385-ea7509332b0b" = "O365SBRM Service"
"9d3e55ba-79e0-4b7c-af50-dc460b81dca1" = "Microsoft Azure Data Catalog"
"9e133cac-5238-4d1e-aaa0-d8ff4ca23f4e" = "Conferencing Virtual Assistant"
"9e4a5442-a5c9-4f6f-b03f-5b9fcaaf24b1" = "OfficeServicesManager"
"9ea1ad79-fdb6-4f9a-8bc3-2b70f96e34c7" = "Bing"
"9f505dbd-a32c-4685-b1c6-72e4ef704cb0" = "Amazon Alexa"
"a0551534-cfc9-4e1f-9a7a-65093b32bb38" = "AzureLockbox"
"a0be0c72-870e-46f0-9c49-c98333a996f7" = "AzureDnsFrontendApp"
"a164aee5-7d0a-46bb-9404-37421d58bdf7" = "Microsoft Teams AuthSvc"
"a1cf9e0a-fe14-487c-beb9-dd3360921173" = "Meetup"
"a232010e-820c-4083-83bb-3ace5fc29d0b" = "Azure Cosmos DB"
"a25dbca8-4e60-48e5-80a2-0664fdb5c9b6" = "Microsoft MileIQ"
"a303894e-f1d8-4a37-bf10-67aa654a0596" = "Compute Usage Provider"
"a393296b-5695-4463-97cb-9fa8638a494a" = "My SharePoint Sites"
"a3dfc3c6-2c7d-4f42-aeec-b2877f9bce97" = "Microsoft Azure AD Identity Protection"
"a43e5392-f48b-46a4-a0f1-098b5eeb4757" = "Cloudsponge"
"a47591ab-e23e-4ffa-9e1b-809b9067e726" = "Microsoft Teams User Profile Search Service"
"a4c95b9e-3994-40cc-8953-5dc66d48348d" = "Microsoft Container Registry"
"a4ee6867-8640-4495-b1fd-8b26037a5bd3" = "Workflow"
"a57aca87-cbc0-4f3c-8b9e-dc095fdc8978" = "IAM Supportability"
"a6aa9161-5291-40bb-8c5c-923b567bee3b" = "Storage Resource Provider"
"a855a166-fd92-4c76-b60d-a791e0762432" = "Microsoft Teams VSTS"
"a8b6bf88-1d1a-4626-b040-9a729ea93c65" = "Compute Artifacts Publishing Service"
"aa0e3dd4-df02-478d-869e-fc61dd71b6e8" = "Workflow"
"aa580612-c342-4ace-9055-8edee43ccb89" = "Microsoft Teams Shifts"
"aa9ecb1e-fd53-4aaa-a8fe-7a54de2c1334" = "Microsoft Office365 Configure"
"aad3e70f-aa64-4fde-82aa-c9d97a4501dc" = "RPA - Machine Management Relay Service"
"aaf214cc-8013-4b95-975f-13203ae36039" = "Power BI Tiles"
"ab27a73e-a3ba-4e43-8360-8bcc717114d8" = "Microsoft OfficeModernCalendar"
"ab3be6b7-f5df-413d-ac2d-abf1e3fd9c0b" = "Microsoft Teams Graph Service"
"ab9b8c07-8f02-4f72-87fa-80105867a763" = "One Drive"
"abba844e-bc0e-44b0-947a-dc74e5d09022" = "Domain Controller Services"
"abfa0a7c-a6b6-4736-8310-5855508787cd" = "Microsoft Azure App Service"
"ac815d4a-573b-4174-b38e-46490d19f894" = "Workflow"
"ad230543-afbe-4bb4-ac4f-d94d101704f8" = "Apiary for Power BI"
"ad40333e-9910-4b61-b281-e3aeeb8c3ef3" = "AI Builder Authorization Service"
"aeb86249-8ea3-49e2-900b-54cc8e308f85" = "M365 License Manager"
"aedca418-a84d-430d-ab84-0b1ef06f318f" = "Workflow"
"b10686fd-6ba8-49f2-a3cd-67e4d2f52ac8" = "NovoEd"
"b1379a75-ce5e-4fa3-80c6-89bb39bf646c" = "Microsoft Teams Chat Aggregator"
"b20d0d3a-dc90-485b-ad11-6031e769e221" = "SalesInsightsWebApp"
"b2590339-0887-4e94-93aa-13357eb510d7" = "Workflow"
"b28ec8e1-950e-4bd0-b3d0-c1e93074b88b" = "Azure Gallery RP"
"b2cc270f-563e-4d8a-af47-f00963a71dcd" = "OneProfile Service"
"b4114287-89e4-4209-bd99-b7d4919bcf64" = "OfficeDelve"
"b4bddae8-ab25-483e-8670-df09b9f1d0ea" = "Signup"
"b4ca0290-4e73-4e31-ade0-c82ecfaabf6a" = "Azure DNS Managed Resolver"
"b503eb83-1222-4dcc-b116-b98ed5216e05" = "Azure Notification Service"
"b51a99a9-ccaa-4687-aa2c-44d1558295f4" = "Microsoft Exact Data Match Upload Agent"
"b55b276d-2b09-4ad2-8de5-f09cf24ffba9" = "Microsoft Teams - Teams And Channels Service"
"b645896d-566e-447e-8f7f-e2e663b5d182" = "OpsDashSharePointApp"
"b692184e-b47f-4706-b352-84b288d2d9ee" = "Microsoft MileIQ RESTService"
"b6b84568-6c01-4981-a80f-09da9a20bbed" = "Microsoft Invoicing"
"b73f62d0-210b-4396-a4c5-ea50c4fab79b" = "Skype Business Voice Fraud Detection and Prevention"
"b7912db9-aa33-4820-9d4f-709830fdd78f" = "ConnectionsService"
"b8066b99-6e67-41be-abfa-75db1a2c8809" = "Microsoft Intune IW Service"
"b815ce1c-748f-4b1e-9270-a42c1fa4485a" = "Workflow"
"b8340c3b-9267-498f-b21a-15d5547fd85e" = "Hyper-V Recovery Manager"
"b861dbcc-a7ef-4219-a005-0e4de4ea7dcf" = "Data Export Service for Microsoft Dynamics 365"
"b8d56525-1fd0-4121-a640-e0ede64f74b5" = "M365CommunicationCompliance"
"b97b6bd4-a49f-4a0c-af18-af507d1da76c" = "Office Shredding Service"
"b9a92e36-2cf8-4f4e-bcb3-9d99e00e14ab" = "Compute Recommendation Service"
"ba23cd2a-306c-48f2-9d62-d3ecd372dfe4" = "OfficeGraph"
"ba9ff945-a723-4ab5-a977-bd8c9044fe61" = "My Staff"
"bb2a2e3a-c5e7-4f0a-88e0-8e01fd3fc1f4" = "CPIM Service"
"bb8f18b0-9c38-48c9-a847-e1ef3af0602d" = "Microsoft Azure ActiveDirectoryIUX"
"bc59ab01-8403-45c6-8796-ac3ef710b3e3" = "Outlook Online Add-in App"
"bd7b778b-4aa8-4cde-8d90-8aeb821c0bd2" = "Workflow"
"bd93b475-f9e2-476e-963d-b2daf143ffb9" = "SQLVMResourceProviderAuth"
"bf26f092-3426-4a99-abfb-97dad74e661a" = "Azure AD Identity Governance"
"bf4fa6bf-d24c-4d1c-8cfd-12063dd646b2" = "AADPremiumService"
"bf7b96b3-68e4-4fd9-b697-637f0f1e778c" = "Universal Store Entitlements Service"
"bf9fc203-c1ff-4fd4-878b-323642e462ec" = "Jarvis Transaction Service"
"c066d759-24ae-40e7-a56f-027002b5d3e4" = "M365DataAtRestEncryption"
"c161e42e-d4df-4a3d-9b42-e7a3c31f59d4" = "Microsoft Intune API"
"c1f33bc0-bdb4-4248-ba9b-096807ddb43e" = "Office Online Third Party Storage"
"c26550d6-bc82-4484-82ca-ac1c75308ca3" = "Office 365 YammerOnOls"
"c27373d3-335f-4b45-8af9-fe81c240d377" = "P2P Server"
"c2ada927-a9e2-4564-aae2-70775a2fa0af" = "OCaaS Client Interaction Service"
"c2f89f53-3971-4e09-8656-18eed74aee10" = "calendly"
"c37c294f-eec8-47d2-b3e2-fc3daa8f77d3" = "Workflow"
"c39c9bac-9d1f-4dfb-aa29-27f6365e5cb7" = "Azure Advisor"
"c44b4083-3bb0-49c1-b47d-974e53cbdf3c" = "Azure Portal"
"c495cfdc-814f-46a1-89f0-657921c9fbe0" = "Azure AD Identity Governance - Dynamics 365 Management"
"c5393580-f805-4401-95e8-94b7a6ef2fc2" = "Office 365 Management APIs"
"c58637bb-e2e1-4312-8a00-04b5ffcd3403" = "SharePoint Online Client Extensibility"
"c5b17a4f-cc6f-4649-9480-684280a2af3a" = "aciapi"
"c606301c-f764-4e6b-aa45-7caaaea93c9a" = "Office Store"
"c6871074-3ded-4935-a5dc-b8f8d91d7d06" = "ISV Portal"
"c728155f-7b2a-4502-a08b-b8af9b269319" = "AAD Request Verification Service - PROD"
"c830ddb0-63e6-4f22-bd71-2ad47198a23e" = "Microsoft To-Do"
"c859ff33-eb41-4ba6-8093-a2c5153bbd7c" = "Workflow"
"c92229fa-e4e7-47fc-81a8-01386459c021" = "Common Data Service User Management"
"c9224372-5534-42cb-a48b-8db4f4a3892e" = "MS Teams Griffin Assistant"
"c9299480-c13a-49db-a7ae-cdfe54fe0313" = "PowerApps-Advisor"
"c98e5057-edde-4666-b301-186a01b4dc58" = "MicrosoftEndpointDLP"
"c9a559d2-7aab-4f13-a6ed-e7e9c52aec87" = "Microsoft Forms"
"c9d254a9-346a-4c00-95eb-950cb62a58f0" = "SmartList Designer"
"ca0a114d-6fbc-46b3-90fa-2ec954794ddb" = "Microsoft Device Management Checkin"
"ca7f3f0b-7d91-482c-8e09-c5d840d0eac5" = "Log Analytics API"
"cb4dc29f-0bf4-402a-8b30-7511498ed654" = "Power BI Premium"
"cbfda01c-c883-45aa-aedc-e7a484615620" = "Microsoft Windows AutoPilot Service API"
"cc15fd57-2c6c-4117-a88c-83b1d56b4bbe" = "Microsoft Teams Services"
"ccf4d8df-75ce-4107-8ea5-7afd618d4d8a" = "Microsoft Intune AAD BitLocker Recovery Key Integration"
"cdccd920-384b-4a25-897d-75161a4b74c1" = "Skype Teams Firehose"
"ce6ff14a-7fdc-4685-bbe0-f6afdfcfa8e0" = "Microsoft Azure Linux Virtual Machine Sign-In"
"cf53fce8-def6-4aeb-8d30-b158e7b1cf83" = "Microsoft Stream Portal"
"cf6c77f8-914f-4078-baef-e39a5181158b" = "Microsoft Teams Settings Store"
"cfa8b339-82a2-471a-a3c9-0fc0be7a4093" = "Azure Key Vault"
"d0597157-f0ae-4e23-b06c-9e65de434c4f" = "Microsoft Teams Task Service"
"d29a4c00-4966-492a-84dd-47e779578fb7" = "Discovery Service"
"d2a0a418-0aac-4541-82b2-b3142c89da77" = "Microsoft Operations Management Suite"
"d2fa1650-4805-4a83-bcb9-cf41fe63539c" = "ACR-Tasks-Prod"
"d32c68ad-72d2-4acb-a0c7-46bb2cf93873" = "Microsoft Activity Feed Service"
"d3590ed6-52b3-4102-aeff-aad2292ab01c" = "Microsoft Office"
"d3ce4cf8-6810-442d-b42e-375e14710095" = "Graph Explorer"
"d4ebce55-015a-49b5-a083-c84d1797ae8c" = "Microsoft Intune Enrollment"
"d52485ee-4609-4f6b-b3a3-68b6f841fa23" = "On-Premises Data Gateway Connector"
"d52792f4-ba38-424d-8140-ada5b883f293" = "AAD Terms Of Use"
"d676e816-a17b-416b-ac1a-05ad96f43686" = "Workflow"
"d6fdaa33-e821-4211-83d0-cf74736489e1" = "Microsoft Service Trust"
"d7097cd1-c779-44d0-8c71-ab1f8386a97e" = "Microsoft Office Licensing Service Agents"
"d73f4b35-55c9-48c7-8b10-651f6f2acb2e" = "MCAPI Authorization Prod"
"d7c17728-4f1e-4a1e-86cf-7e0adf3fe903" = "Workflow"
"d82073ec-4d7c-4851-9c5d-5d97a911d71d" = "Kaizala Sync Service"
"d87dcbc6-a371-462e-88e3-28ad15ec4e64" = "Domain Controller Services"
"d8877f27-09c0-43aa-8113-40151dae8b14" = "Microsoft Intune AndroidSync"
"d88a361a-d488-4271-a13f-a83df7dd99c2" = "IDML Graph Resolver Service and CAD"
"d8c767ef-3e9a-48c4-aef9-562696539b39" = "Request Approvals Read Platform"
"da109bdd-abda-4c06-8808-4655199420f8" = "Glip Contacts"
"db040338-7cb4-44df-a22b-785bde7ce0e2" = "RPA - Machine Management Relay Service - Application"
"db55028d-e5ba-420f-816a-d18c861aefdf" = "Microsoft Office Licensing Service vNext"
"dbc36ae1-c097-4df9-8d94-343c3d091a76" = "Service Encryption"
"dbcbd02a-d7c4-42fb-8c27-b07e5118b848" = "Azure Graph"
"dbf08535-1d3b-4f89-bf54-1d48dd613a61" = "Workflow"
"dc3294af-4679-418f-a30c-76948e23fe1c" = "Microsoft Kaizala"
"dcad865d-9257-4521-ad4d-bae3e137b345" = "Microsoft SharePoint Online - SharePoint Home"
"ddbf3205-c6bd-46ae-8127-60eb93363864" = "Microsoft Azure Batch"
"de096ee1-dae7-4ee1-8dd5-d88ccc473815" = "MileIQ Admin Center"
"de17788e-c765-4d31-aba4-fb837cfff174" = "Skype for Business Management Reporting and Analytics"
"de926fbf-e23b-41f9-ae15-c943a9cfa630" = "Microsoft Azure Authorization Private Link Provider"
"dee7ba80-6a55-4f3b-a86c-746a9231ae49" = "Microsoft AppPlat EMA"
"df09ff61-2178-45d8-888c-4210c1c7b0b2" = "O365 UAP Processor"
"dff9b531-6290-4620-afce-26826a62a4e7" = "DocuSign"
"e08ab642-962a-4175-913c-165f557d799a" = "teams contacts griffin processor"
"e0ee12cb-2032-40fc-a44f-d6d9f3fad1eb" = "Android qq"
"e1335bb1-2aec-4f92-8140-0e6e61ae77e5" = "CIWebService"
"e1829006-9cf1-4d05-8b48-2e665cb48e6a" = "Microsoft Teams Web Client"
"e1979c22-8b73-4aed-a4da-572cc4d0b832" = "App Studio for Microsoft Teams"
"e3335adb-5ca0-40dc-b8d3-bedc094e523b" = "SubscriptionRP"
"e3583ad2-c781-4224-9b91-ad15a8179ba0" = "Microsoft ExtensibleRealUserMonitoring"
"e3bfd6ac-eace-4438-9dc1-eed439e738de" = "MicrosoftMigrateProject"
"e3c5dbcd-bb5f-4bda-b943-adc7a5bbc65e" = "Workflow"
"e406a681-f3d4-42a8-90b6-c2b029497af1" = "Azure Storage"
"e48d4214-364e-4731-b2b6-47dabf529218" = "skype web"
"e4ab13ed-33cb-41b4-9140-6e264582cf85" = "Azure SQL Database Backup To Azure Backup Vault"
"e691bce4-6612-4025-b94c-81372a99f77e" = "Boomerang"
"e6acb561-0d94-4287-bd3a-3169f421b112" = "Tutum"
"e6f9f783-1fdb-4755-acaf-abed6c642885" = "Meru19 MySQL First Party App"
"e8ab36af-d4be-4833-a38b-4d6cf1cfd525" = "Microsoft Social Engagement"
"e933bd07-d2ee-4f1d-933c-3752b819567b" = "Azure Monitor Control Service"
"e935b4a5-8968-416d-8414-caed51c782a9" = "MicrosoftGuestConfiguration"
"e95d8bee-4725-4f59-910d-94d415da51b9" = "Skype for Business Name Dictionary Service"
"e9f49c6b-5ce5-44c8-925d-015017e9f7ad" = "Azure Data Lake"
"ea2f600a-4980-45b7-89bf-d34da487bda1" = "Microsoft Azure DomainRegistration"
"ea890292-c8c8-4433-b5ea-b09d0668e1a6" = "Azure Credential Configuration Endpoint Service"
"eacba838-453c-4d3e-8c6a-eb815d3469a3" = "Microsoft Flow CDS Integration Service TIP1"
"eace8149-b661-472f-b40d-939f89085bd4" = "Substrate Instant Revocation Pipeline"
"eaf8a961-f56e-47eb-9ffd-936e22a554ef" = "DevilFish"
"ec245c98-4a90-40c2-955a-88b727d97151" = "Azure AD Identity Governance - User Management"
"ecd6b820-32c2-49b6-98a6-444530e5a77a" = "Intune"
"ed9fe1ef-25a4-482f-9981-2b60f91e2448" = "Workflow"
"ef4a2a24-4b4e-4abf-93ba-cc11c5bd442c" = "Edmodo"
"ef5d5c69-a5df-46bb-acaf-426f161a21a2" = "Managed Service Identity"
"ef947699-9b52-4b31-9a37-ef325c6ffc47" = "Power Query Online GCC-L4"
"f09d1391-098c-47d7-ac7e-6ed2afc5016b" = "MicrosoftAzureADFulfillment"
"f0ae4899-d877-4d3c-ae25-679e38eea492" = "AAD App Management"
"f16c4a38-5aff-4549-8199-ee7d3c5bd8dc" = "Workflow"
"f217ad13-46b8-4c5b-b661-876ccdf37302" = "Attach OneDrive files to Asana"
"f25a7567-8ec5-4582-8a65-bfd66b0530cc" = "ReportReplica"
"f2c304cf-8e7e-4c3f-8164-16299ad9d272" = "Azure Management Groups"
"f3b07414-6bf4-46e6-b63f-56941f3f4128" = "Power Query Online"
"f416c5fc-9ac4-4f66-a8e5-cb203139cbe4" = "OCPS Admin Service"
"f53895d3-095d-408f-8e93-8f94b391404e" = "Portfolios"
"f5aeb603-2a64-4f37-b9a8-b544f3542865" = "Microsoft Teams RetentionHook Service"
"f5c26e74-f226-4ae8-85f0-b4af0080ac9e" = "Application Insights API"
"f6b60513-f290-450e-a2f3-9930de61c5e7" = "Microsoft Azure Log Search Alerts"
"f7069a8d-9edc-4300-b365-ae53c9627fc4" = "Microsoft MileIQ Dashboard"
"f8d98a96-0999-43f5-8af3-69971c7bb423" = "iOS Accounts"
"f9d02341-e7aa-456d-926d-4a0ca599fbee" = "Office 365 Enterprise Insights"
"fa7ff576-8e31-4a58-a5e5-780c1cd57caa" = "OneNote"
"fbb0ac1a-82dd-478b-a0e5-0b2b98ef38fe" = "Substrate-FileWatcher"
"fbc197b7-9e9c-4f98-823f-93cb1cb554e6" = "Capacity"
"fc03f97a-9db0-4627-a216-ec98ce54e018" = "Azure AD Notification"
"fc68d9e5-1f76-45ef-99aa-214805418498" = "Azure AD Identity Protection"
"fc75330b-179d-49af-87dd-3b1acf6827fa" = "AzureAutomationAADPatchS2S"
"fc780465-2017-40d4-a0c5-307022471b92" = "WindowsDefenderATP"
"fdc83783-b652-4258-a622-66bc85f1a871" = "FedExPackageTracking"
"fe217466-5583-431c-9531-14ff7268b7b3" = "Microsoft Education"
}
$obj=($table.GetEnumerator()|where key -Match "$in")
write-host $obj.key = $obj.value
$obj.key,$obj.value-join"$([char]34)`r`n$([char]34)"-replace "^",'"'-replace"$",'"'|Set-Clipboard