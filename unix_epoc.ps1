$epoch=$args[0]
$len="$epoch"
if ($len.Length -eq 10){Write-Host (get-date "1/1/1970").AddSeconds($epoch) "(seconds precision)"}
elseif ($len.Length -eq 13){Write-Host (get-date "1/1/1970").AddMilliseconds($epoch) "(miliseconds precision)"}
else {Write-Host "ERROR: Something is wrong with the length of this UNIX epoch string. It should be like 10 or 13 characters long."}