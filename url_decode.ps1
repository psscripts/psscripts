Add-Type -AssemblyName System.Web
$urlTodDecode = $args[0]
$decodedURL = [System.Web.HttpUtility]::UrlDecode($urlTodDecode)
Write-Host $decodedURL -ForegroundColor Green
Set-Clipboard $decodedURL