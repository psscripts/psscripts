Add-Type -AssemblyName System.Web
$webURL=Read-Host
$urlToEncode = $webURL
$encodedURL = [System.Web.HttpUtility]::UrlEncode($urlToEncode) 
Write-Host $encodedURL -ForegroundColor Green